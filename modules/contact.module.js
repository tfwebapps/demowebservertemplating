const contacts = [
    { id : 1, nom : "Defer", prenom : "Théophile", email : "theo.d@gmail.com", tel : "0475757575"}
]

let currentContactId = 1

const getAll = () => {
    return contacts
}

const createContact = (contact) => {
    contacts.push({...contact, id : ++currentContactId })
    return contact //Idéalement renvoyé le contacté nouvellement crée pour avoir l'id
}

module.exports = { getAll, createContact }