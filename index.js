const http = require("http")
const urlParser = require("url")
const queryString = require("querystring")

const ejs = require("ejs")
const path = require("path")

//Pour gérer les fichiers (css, images)
const fs = require("fs")

//Nos modules
const contactModule = require("./modules/contact.module")

const app = http.createServer((req, res) => {
    const reqUrl = urlParser.parse(req.url).pathname
    const reqQuery = urlParser.parse(req.url).query
    const reqMethod = req.method

    console.log(reqUrl);

    // const fileInPublic = path.resolve(process.cwd(), reqUrl)
    const fileInPublic = process.cwd()+'/public'+reqUrl
    console.log(fileInPublic);
    if(reqUrl !== '/' && fs.existsSync(fileInPublic)) {
        console.log(`Le fichier ${fileInPublic} existe`)

        const file = fs.readFileSync(fileInPublic)
        
        //On récupère l'extension pour savoir comment gérer ce fichier
        const extension = path.extname(fileInPublic).replace('.', '')
        console.log("EXTENSION : ", extension);

        let contentType = ''
        if(extension === 'css') {
            contentType = 'text/css'
        } 
        else if(['jpg', 'jpeg', 'png', 'gif', 'svg', 'webp', 'bmp'].includes(extension)) {
            contentType = 'image/'+extension
        }

        console.log(contentType);

        res.writeHead(200, {
            "Content-type" : contentType
        })
        res.end(file)
        return
    }


    if( reqUrl === "/" && reqMethod === 'GET' ) {
        console.log('Bienvenue sur la page d\'accueil')

        const trainers = [
            { firstname : "Aude", lastname : "Beurive" },
            { firstname : "Pierre", lastname : "Santos"},
            { firstname : "Gavin", lastname : "Chaineux"},
            { firstname : "Steve", lastname : "Lorent" }
        ]        
        const TODAY = new Date().toLocaleDateString('fr-BE', { dateStyle : "full" })
        //Création du contenu HTML à envoyer en réponse
        const filePath = path.resolve("views", "home.ejs") 
        const data = { trainers , today : TODAY }

        ejs.renderFile(filePath, data, (err, render) => {
            if(err) {
                console.log(err)
                return
            }
            res.end(render)
        } )

    } else if ( reqUrl === "/contact") {
        if(reqMethod === "GET") {
            
            const filePath = path.resolve("views", "contact.ejs")
            ejs.renderFile(filePath, (err, render) => {
                if(err) {
                    console.log(err);
                    return
                }
                res.end(render)
            })
        }
        if(reqMethod === "POST") {
            let data = ''
            req.on('data', (form) => {
                data += form.toString('utf-8')
            })

            req.on('end', () => {
                const messageToAdd = queryString.parse(data)
                //TODO (idéalement mais on va le faire because c'est chiant en node pur) -> Ajouter en db

                res.writeHead(301 , {
                    "Location" : "/"
                })
                res.end()
            })
        }

    } else if( reqUrl === "/list-contact" && reqMethod === "GET") {
        const fileName = path.resolve("views", "list-contact.ejs")

        const data = {
            contacts : contactModule.getAll()
        }

        ejs.renderFile(fileName, data, (err, render) => {
            if(err) {
                return
            }
            res.end(render)
        })

    }  else if (reqUrl === "/add-contact") {
        if(reqMethod === "GET") {
            const fileName = path.resolve("views", "add-contact.ejs")
            ejs.renderFile(fileName, (err, render) => {
                if(err) {
                    console.log(err);
                    return
                }
                res.end(render)
            })
        }
        if(reqMethod === "POST") {
            let data = ''
            req.on('data', (form) => {
                data += form.toString()
            })

            req.on("end", () => {
                const dataToObject = queryString.parse(data)
                const contactToAdd = {
                    nom : dataToObject.nom,
                    prenom : dataToObject.prenom,
                    email : dataToObject.email,
                    tel : dataToObject.tel,
                }
                contactModule.createContact(contactToAdd)

                res.writeHead(301, {
                    "Location" : "/list-contact"
                })
                res.end()
            })
        }
    }
    
    
    else {
        const fileName = path.resolve("views", "not-found.ejs")
        ejs.renderFile(fileName, (err, render) => {
            if(err) {
                return
            }
            res.end(render)
        })
    }

})

app.listen(8080, () => {
    console.log('Server started on port 8080')
})